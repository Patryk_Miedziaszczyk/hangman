import random
import re
mistakes = 0
list_words = ["długoterminowy", "dobrodziejstwo", "mezopotamia", "międzynarodowy", "monotematyczność",
              "brzęczyszczykiweicz", "interpretować", "grzecznościowy"]
polish = "[AaĄąBbCcĆćDdEeĘęFfGgHhIiJjKkLlŁłMmNnŃńOoÓóPpRrSsŚśTtUuWwYyZzŹźŻż]"
index = random.randrange(0,8)
word = list_words[index]
length = len(word)
guessing = re.sub(polish, "_", word)
guessing = list(guessing)
word = list(word)
string_guessing = "_"
while True:
    character = input("Podaj znak: ")
    while True:
        if character in word:
            occurences = word.count(character)
            if occurences > 1:
                for i in range(occurences):
                    place = word.index(character)
                    word[place] = "_"
                    guessing[place] = character
                    string_guessing = " ".join(guessing)
            else:
                place = word.index(character)
                word[place] = "_"
                guessing[place] = character
                string_guessing = " ".join(guessing)
            print(string_guessing)
            break
        else:
            mistakes += 1
            print("Ups, błąd:  " + str(mistakes) + "\n")
            break

    if mistakes == 10:
        print("Koniec gry, przegrałeś")
        break

    if string_guessing.count("_") == 0:
        print("Koniec gry, wygrałeś")
        break